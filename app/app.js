(function(angular) {
    'use strict';
    var myApp = angular.module('myApp', ['ngRoute', 'ngStorage']);
    myApp.config(function($routeProvider,$locationProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "views/listUsers.html",
            })
            .when("/addUser", {
                templateUrl: "views/addUser.html",
            })
            .when("/profile/:id", {
                templateUrl: "views/profile.html",
            })
            .otherwise({
                redirect:'/'
            })
        $locationProvider.html5Mode(true);    
    });    
    
})(window.angular);   



