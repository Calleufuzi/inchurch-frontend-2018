(function(angular) {
    'use strict';
    var myApp = angular.module('myApp');
    myApp.controller('MainController', function($scope, UserCrud, $localStorage, $routeParams, localUserService, $location, $timeout){
        var vm = this;
        $scope.users = [];
        $scope.user = {};
        $scope.newUsers = [];
        $scope.newUser = {};
        $scope.innerUser = {};
        $scope.id = $routeParams.id;
        $scope.dataLoaded = true;
        $scope.isList = true;
        $scope.isAdd = false;

         // Go to Profile View
         $scope.goToProfile = function(user){
            localUserService.inneUser = user;
            $location.path( '/profile/'+user.id)
            $scope.innerUser = localUserService.inneUser;
        }

        // Go to List View
        $scope.goToList = function(){
            $timeout(function () {
                $location.path('/')
            }, 800);
        };
        $scope.isSelected = function(type){
            if (type === 'list') {
                $scope.isList = true;
                $scope.open = false;
                $scope.isAdd = false;
            }else{
                $scope.isAdd = true;
                $scope.open = false 
                $scope.isList = false
            }
        }

        // List User
        $scope.listUsers = function(){
            UserCrud.list().then(function(users){
                $localStorage.users = users;
                $scope.users = $localStorage.users;
                $timeout(function () {
                    $scope.dataLoaded = false;
                }, 100);
            });
        };

        // User add Function    
        $scope.addUser = function(user) {
            var user = {
                name: user.name,
                job: user.job,
            }

            UserCrud.add(user).then(function(user){
                $scope.newUser = user;
                $scope.newUsers.push(user)
                console.log($scope.newUsers);
                $('#addSuccess').modal('toggle');
                $scope.user = {};
            })
        }; 

        // User edit function
        $scope.editUser = function(user, id){
            var user = {
                name: user.newName,
                job: user.newJob,
            }

            UserCrud.edit(user, id).then(function(user){
                console.log(user);
                $('#editModal').modal('hide');
                $scope.user = {};
            })
            
        }; 

        // User remove function
        $scope.removeUser = function(id){
            UserCrud.remove(id)
            $('#removeSuccess').modal('toggle', function(e){
                $location.path( '/users/')
            });
            
        };               
            
    });

    // Service to Storage User
    myApp.service("localUserService", function() {
        return {
          user: {}
        }
      });    
})(window.angular);