(function(angular) {
    'use strict';
    
    var myApp = angular.module('myApp');
    myApp.factory("UserCrud", function($q, $http){
        return {
            list: function() {
                var p = $q.defer();
                var users = [];
                $http.get("https://reqres.in/api/users?page=1").then(
                    function (result) {
                        console.log(result);
                        angular.forEach(result.data.data, function(user, id) {
                            users.id = id;
                            users.push(user)
                        });
                        p.resolve(users)
                    }
                )
                $http.get("https://reqres.in/api/users?page=2").then(
                    function (result) {
                        console.log(result);
                        angular.forEach(result.data.data, function(user, id) {
                            users.id = id;
                            users.push(user)
                        });
                        p.resolve(users)
                    }
                )
                $q.all(users);
                return p.promise;
            },
            add: function(user) {
                var p = $q.defer();
                var data = { 
                    name : user.name,
                    job : user.job,
                }
                $http.post('https://reqres.in/api/users', data)
                    .then(function (result) {
                        console.log(result)
                        p.resolve(result.data)
                    })
                    .catch(function(erro){
                        console.log(erro);
                        
                    })
                return p.promise;
            },
            edit: function(user,id){
                var p = $q.defer();
                var data = { 
                    name : user.name,
                    job : user.job,
                }
                console.log(data);
                
                $http.put('https://reqres.in/api/users/'+id, data)
                    .then(function (result) {
                        console.log(result)
                        p.resolve(result.data)
                    })
                    .catch(function(erro){
                        console.log(erro);
                        
                    })
                return p.promise;
            },
            remove: function(id){
                var p = $q.defer();
                $http.delete('https://reqres.in/api/users/'+id)
                    .then(function (result) {
                        console.log(result)
                        p.resolve(result.data)
                    })
                    .catch(function(erro){
                        console.log(erro);
                        
                    })
                return p.promise;
            }
        }    
    });

})(window.angular);   